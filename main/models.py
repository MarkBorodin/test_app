from django.db import models


class News(models.Model):
    title = models.CharField('title', max_length=200, null=False, blank=False)
    link = models.URLField(max_length=200, null=False, blank=False,)
    creation_date = models.DateTimeField(auto_now=True)
    amount_of_upvotes = models.SmallIntegerField(default=0)
    author_name = models.CharField('author_name', max_length=100, null=False, blank=False, default='unknown author')

    def __str__(self):
            return self.title

    class Meta:
        verbose_name = 'News'
        verbose_name_plural = 'News'


class Comment(models.Model):
    news = models.ForeignKey(News, on_delete=models.CASCADE)
    author_name = models.CharField('author_name', max_length=100, null=False, blank=False, default='unknown author')
    content = models.TextField('comment', max_length=500, null=False, blank=False)
    creation_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.content

    class Meta:
        verbose_name = 'Comment'
        verbose_name_plural = 'Comments'
