from django.shortcuts import redirect

from .models import News, Comment
from .forms import NewsForm, CommentForm
from django.views.generic import DeleteView, CreateView, UpdateView, DetailView, ListView
from django.urls import reverse_lazy


class Index(ListView):
    model = News
    form_class = NewsForm
    context_object_name = 'news_list'
    template_name = 'main/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['news'] = News.objects.all()
        context['comments'] = Comment.objects.all()
        return context


class NewsCreateView(CreateView):
    model = News
    form_class = NewsForm
    success_url = reverse_lazy('index')
    template_name = 'main/create_news.html'


class NewsUpdateView(UpdateView):
    model = News
    form_class = NewsForm
    context_object_name = 'news'
    success_url = reverse_lazy('index')
    template_name = 'main/update_news.html'


class NewsDetailView(DetailView):
    model = News
    context_object_name = 'news'
    template_name = 'main/detail_news.html'


class NewsDeleteView(DeleteView):
    model = News
    success_url = reverse_lazy('index')
    template_name = 'main/delete_news.html'


class CommentCreateView(CreateView):
    model = Comment
    form_class = CommentForm
    success_url = reverse_lazy('index')
    template_name = 'main/create_comment.html'


class CommentUpdateView(UpdateView):
    model = Comment
    form_class = CommentForm
    context_object_name = 'comment'
    success_url = reverse_lazy('index')
    template_name = 'main/update_comment.html'


class CommentDeleteView(DeleteView):
    model = Comment
    success_url = reverse_lazy('index')
    template_name = 'main/delete_comment.html'


def to_vote(request, pk):
    result = News.objects.get(id=pk)
    result.amount_of_upvotes += 1
    result.save()
    return redirect('index')
