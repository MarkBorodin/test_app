from .models import News, Comment
from django import forms
from django.forms import ModelForm


class NewsForm(ModelForm):
    class Meta:
        model = News
        fields = ['title', 'link', 'author_name']


class CommentForm(ModelForm):
    news = forms.ModelChoiceField(queryset=News.objects.all())

    class Meta:
        model = Comment
        fields = ['content', 'author_name', 'news']
