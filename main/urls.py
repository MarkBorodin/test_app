from django.urls import path
from .views import Index, NewsCreateView, NewsDeleteView, NewsDetailView, NewsUpdateView, \
    CommentCreateView, CommentDeleteView, CommentUpdateView, to_vote

urlpatterns = [
    path('', Index.as_view(), name='index'),
    path('news_create', NewsCreateView.as_view(), name='news_create'),
    path('news_delete/<int:pk>', NewsDeleteView.as_view(), name='news_delete'),
    path('news_detail/<int:pk>', NewsDetailView.as_view(), name='news_detail'),
    path('news_update/<int:pk>', NewsUpdateView.as_view(), name='news_update'),
    path('comment_create/<int:pk>', CommentCreateView.as_view(), name='comment_create'),
    path('comment_delete/<int:pk>', CommentDeleteView.as_view(), name='comment_delete'),
    path('comment_update/<int:pk>', CommentUpdateView.as_view(), name='comment_update'),
    path('to_vote/<int:pk>', to_vote, name='to_vote'),
]
